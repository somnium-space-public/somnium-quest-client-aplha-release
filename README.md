# Welcome to Somnium Quest Client alpha version release repository.
1. in order to download a release look on your left under Deployment -> [Releases](https://gitlab.com/somnium-space-public/somnium-quest-client-alpha-release/-/releases)
2. select the latest release
3. download the apk through APK download link
4. if this is your first time refer to [this guide on how to sideload with sidequest](https://uploadvr.com/sideloading-quest-how-to/) 
5. launch the app under the apps unknown sources
6. Enjoy

# Release notes

## 0.0.1-alpha
DISCLAIMER: this is an alpha version expect a lot of bugs and visual artifacts!

### features: 
Offline lobby: 
- users can look at for now static previews of our best user created parcels
- users can log in to theirs existing Somnium Account
- launch from lobby and go through portal

Crossplatform:
- fully working voice integration
- tracking of players from Quest client to VR/PC clients and vice versa
- Player tags(icons and names)
- achievments

MainSquare:
- Includes buildings from VR/PC client that are accesible: Somnium Headquarters, Somnium Mall, Arcade hall, Main square
- Also includes Planetarium without interior (for now)

Climbing:
- basic implementation of climbing

Teleport:
- teleport by pressing thumbstick pointing at the place you want to teleport to and release

Avatars:
- only 2 random avatars are active at the moment for somnium avatars
- for custom avatars we use outline of default somnium avatar
